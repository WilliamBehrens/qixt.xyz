package xyz.qixt

// General deps
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.response.*
import io.ktor.routing.*

// Frontend imports
import com.github.mustachejava.*
import com.github.mustachejava.codes.DefaultMustache
import io.ktor.mustache.Mustache
import io.ktor.mustache.Mustache.*
import io.ktor.mustache.MustacheContent
import io.ktor.mustache.MustacheContent.*
import io.ktor.mustache.*

// Static content
import io.ktor.http.content.*
import java.io.*

@Suppress("unused") // Referenced in application.conf
//@kotlin.jvm.JvmOverloads

fun main() {
    
    embeddedServer(Netty, port = 8000) {
        
        install(Routing)
        install(Mustache) {
            mustacheFactory = DefaultMustacheFactory("resources/views")
        }   

        routing {
            
            static("assets") {
                files("resources/public/styles")
                files("resources/public/images")
            }

            get("/") {
                val info = HomeInfo("Will", 1)
                call.respond(MustacheContent("resources/views/index.hbs", mapOf("info" to info)))           
            }
    
        }

    }.start(wait = true)

}

data class HomeInfo(var name: String, var id: Int)
